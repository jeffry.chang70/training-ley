package Constant

//elegant constant
type StatusOrderType int
type ErrConstant string

const (
	InvalidTableExists ErrConstant = "Meja sudah ada , jangan di beli"
)

const (
	Process StatusOrderType = iota
	Pickup
	Delivering
	OrderDone
)

func (constantStatus StatusOrderType) Transporter() string {
	statusString := map[StatusOrderType]string{
		Process:    "Order incoming",
		Pickup:     "Order is loaded",
		Delivering: "Order is delivering",
		OrderDone:  "Order already done",
	}
	result, exist:= statusString[constantStatus]
	if !exist{
		return "Data not found"
	}
	return result
}

func (constantStatus StatusOrderType) Cargo() string {
	statusString := map[StatusOrderType]string{
		Process:    "Order is process",
		Pickup:     "Order is pickup",
		Delivering: "Order is delivering",
		OrderDone:  "Order already received",
	}
	result, exist:= statusString[constantStatus]
	if !exist{
		return "Data not found"
	}
	return result
}
