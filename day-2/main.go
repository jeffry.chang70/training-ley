package main

import (
	"fmt"
	"training-ley/day-2/Constant"
	"training-ley/day-2/User"
)

func main() {
	// hash map
	// pointer
	// struct
	// constant

	//data dari DB
	orderStatus := 4
	role:= "Transporter"
	////FE need order status string
	statusString := Constant.StatusOrderType(orderStatus).Transporter()
	if role == "Cargo"{
		statusString = Constant.StatusOrderType(orderStatus).Cargo()
	}
	fmt.Println("Status Order : ",statusString)
	//
	////pointer
	namaPeminjam := "Agus"
	var nama *string
	nama = &namaPeminjam
	if nama != nil {
		PergantianNamaPeminjam(nama)
		fmt.Println("nama 2: ",*nama)
		fmt.Println("nama 1: ",namaPeminjam)
	}
	//
	//// hash map
	responseMap := make(map[string]interface{})
	responseMap["name"] = "Agus"
	responseMap["age"] = 10
	responseMap["salary"] = 1000.1
	responseMap["married"] = false
	fmt.Println("MAP : ",responseMap)

	DoNumber := []string{"DO-001", "DO-002", "DO-001"}
	tempMap := make(map[string]bool)
	for _, value := range DoNumber {
		if _, exists := tempMap[value] ; exists {
			fmt.Println("DUPLICATE DO : ",value)
		}
		tempMap[value] = true
	}
	fmt.Println("MAP : ",tempMap)
	var listUser []User.MasterUser

	var msUser User.MasterUser
	//data 1
	msUser.Request.Username = "Agus"
	msUser.Request.Password = "Agus1234"
	msUser.Request.Gender = "Male"
	msUser.Request.Age = 10
	listUser = append(listUser, msUser)

	//data 2
	msUser.Request.Username = "Agus"
	msUser.Request.Password = "Agus1234"
	msUser.Request.Gender = "Male"
	msUser.Request.Age = 10
	msUser.Request.Company.CompanyName = "Pt Logkar"
	msUser.Request.Company.CompanySection = "Transportation"
	msUser.Request.Company.ParentCompany.CompanyName = "Wilmar"
	msUser.Request.Company.ParentCompany.CompanySection = "Minyak"
	listUser = append(listUser, msUser)

	fmt.Println("LIST BEFORE UPDATE: ", listUser)

	updateUserDataMap := make(map[string]User.MasterUser)

	for index, val := range listUser {
		if _, exist := updateUserDataMap[val.Request.Username]; exist &&
			updateUserDataMap[val.Request.Username].Request.Company.CompanyName == "" {
			fmt.Println("MASOK SINI : ", updateUserDataMap[val.Request.Username], " INDEX : ",updateUserDataMap[val.Request.Username].Index)
			updateUserDataMap[val.Request.Username] = val
			listUser[updateUserDataMap[val.Request.Username].Index] = val
			fmt.Println("MASOK SINI 2: ", updateUserDataMap[val.Request.Username])
		}
		val.Index = index
		updateUserDataMap[val.Request.Username] = val
	}
	fmt.Println("LIST AFTER UPDATE: ", listUser)
}

func PergantianNamaPeminjam(nama *string) {
	*nama = "jefry"
}

func CheckPalindrom(data string) (isPalindrom bool) {
	var tempData string
	for i := len(data); i > 0; i-- {
		tempData += string(data[i-1])
	}
	if tempData == data {
		return true
	}
	return false
}

func CheckIsExistsFurniture(dataFurniture []string) (bool, Constant.ErrConstant) {
	for _, value := range dataFurniture {
		if value == "Meja" {
			return true, Constant.InvalidTableExists
		}
	}
	return false, "Belom ada Furniture"
}
