package User

type MasterUser struct {
	Request dataUser `json:"request"`
	Index int
}

type dataUser struct {
	Username string  `json:"username"`
	Password string  `json:"password"`
	Gender   string  `json:"gender"`
	Age      int     `json:"age"`
	Company  company `json:"company"`
}

type company struct {
	companyAttribute
	ParentCompany companyAttribute `json:"parent_company"`
}

type companyAttribute struct {
	CompanyName    string `json:"company_name"`
	CompanySection string `json:"company_section"`
}
