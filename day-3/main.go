package main

import (
	"fmt"
)

type Pengiriman interface {
	PricingExpressPackage() int
	PricingRegularPackage() int
}

type JNE struct {
	JenisBarang string
	BeratBarang int
	Harga       int
}

func (j JNE) PricingExpressPackage() int {
	fmt.Println("HALO TEST")
	harga := 10000
	if j.BeratBarang >= 1 && j.BeratBarang <= 10 {
		fmt.Println("MASOK")
		harga = 5000
	}
	return harga
}

func (j JNE) PricingRegularPackage() int {
	harga := 5000
	if j.BeratBarang > 1 || j.BeratBarang < 10 {
		harga = 3000
	}
	return harga
}

type Sicepat struct {
	JenisBarang string
	BeratBarang int
	Harga       int
}

func (s Sicepat) PricingExpressPackage() int {
	harga := 4000
	if s.BeratBarang < 10 {
		harga = 2000
	}
	return harga
}

func (s Sicepat) PricingRegularPackage() int {
	harga := 2000
	if s.BeratBarang > 1 || s.BeratBarang < 10 {
		harga = 1000
	}
	return harga
}

func (j Sicepat) PricingExtraPackage() int {

	return 0
}

type Tiki struct {
	JenisBarang string
	BeratBarang int
	Harga       int
}

func (t Tiki) PricingExpressPackage() int {
	harga := 3000
	if t.BeratBarang > 1 || t.BeratBarang < 10 {
		harga = 2000
	}
	return harga
}

func (t Tiki) PricingRegularPackage() int {
	harga := 2500
	if t.BeratBarang > 1 || t.BeratBarang < 10 {
		harga = 1500
	}
	return harga
}

func Director(beratBarang int, jenisBarang string, companyLogistic string) Pengiriman {
	if companyLogistic == "JNE" {
		return &JNE{
			JenisBarang: jenisBarang,
			BeratBarang: beratBarang,
		}
	}else if companyLogistic == "TIKI" {
		return &Tiki{
			JenisBarang: jenisBarang,
			BeratBarang: beratBarang,
		}
	}
	return &Sicepat{
		JenisBarang: jenisBarang,
		BeratBarang: beratBarang,
	}
}

func main() {
	//Interface
	//JNE , Sicepat, TIKI
	//Kilat
	//Regular
	//var pricingLogistic Pengiriman
	//var dataJNE JNE
	//dataJNE.BeratBarang = 9
	//dataJNE.SatuanBarang = "Kg"
	//pricingLogistic = dataJNE
	//fmt.Println("JNE EXPRESS: ", pricingLogistic.PricingExpressPackage())
	//fmt.Println("JNE Reguler: ", pricingLogistic.PricingRegularPackage())


	//customer mau kirim barang ganja 10kg
	//sync.WaitGroup{}
	go func(){
		Director(11,"Ganja","JNE").PricingExpressPackage()
	}()

	fmt.Println("HALO TEST 2")
}
